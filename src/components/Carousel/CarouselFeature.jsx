import React from 'react';
import { Carousel } from 'react-responsive-carousel';
import 'react-responsive-carousel/lib/styles/carousel.min.css';
import productF1 from '../../utilities/product-feature-1.jpg';
import productF2 from '../../utilities/product-feature-2.jpg';

export default function CarouselComponent() {
  return (
    <div class="carousel-wrapper">
      <Carousel>
        <div>
          <img src={productF1} />
        </div>
        <div>
          <img src={productF2} />
        </div>
      </Carousel>
    </div>
  );
}
