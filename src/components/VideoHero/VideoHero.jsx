import React from "react";
import "./VideoHero.style.scss";

export default function VideoHero() {
  return (
    <div className={"container"}>
      <div className="row vh-sos-wrapper ">
        <div className="vh-sos-text-wrapper">
          <span className="vh-sos-p1">Story of </span>
          <span className="vh-sos-p2">Scarlett</span>
          <div className="divider-container">
            <div>
              <hr className="red-divider" />
            </div>
            <div className="ml-3 pt-1 divider-text">Radio Dj</div>
          </div>
        </div>
        <div className="offset-sm-2 col-md-10">
          <div className={"embed-responsive embed-responsive-16by9"}>
            <iframe
              width={"100%"}
              className={"embed-responsive-item"}
              src="//www.youtube.com/embed/noZPyS8_W5k"
              allowfullscreen
            ></iframe>
          </div>
        </div>
      </div>
    </div>
  );
}
