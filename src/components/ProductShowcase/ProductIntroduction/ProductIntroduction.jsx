import React from 'react';
import product from '../../../utilities/story_of_scarlett.jpg';
import './ProductIntroduction.style.scss';

export default function ProductIntroduction() {
  return (
    <section id={'info'} class={'py-5'}>
      <div className={'container'}>
        <div className={'row'}>
          <div className={'col-md-6 align-self-center'}>
            <div className="pi-sos-text-wrapper pb-2">
              <span className="pi-sos-p1">Story of </span>
              <span className="pi-sos-p2">Scarlett</span>
            </div>
            <p className="pi-text text-center text-md-left">
              <em>
                “ Introducing our Curl and Straight Confidence influencer,
                @scarletthoward Scarlett is a model, music lover and a DJ with a
                radio show on Radio Essex. Proud of her long, red hair, Scarlett
                gets fans from all over the world contacting her for tips about
                styling and being a red-head. reminington.in/2G3wrYS ”
              </em>
            </p>
            <div className="small-link pt-4 mb-5  text-center text-md-left">
              Product Info <i className="fa fa-long-arrow-alt-right ml-2"></i>{' '}
            </div>
          </div>
          <div className={'col-md-6'}>
            <img src={product} alt="product" className={'img-fluid'} />
          </div>
        </div>
      </div>
    </section>
  );
}
