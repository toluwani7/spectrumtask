import React from "react";
import "./ProductCarouselContent.style.scss";
import productHeader from "../../../utilities/product-header.png";
import smallHeader from "../../../utilities/product-collection-main.png";
export default function ProductCarouselContent() {
  return (
    <div className="container">
      <div className="row">
        <div className={"col-12 product-header"}>
          <img
            src={productHeader}
            className={"img-fluid"}
            alt={"alt product"}
          />
          <h1>PRODUCT COLLECTION</h1>
          <img src={smallHeader} className={"img-fluid"} alt={"alt product"} />
        </div>
      </div>
    </div>
  );
}
