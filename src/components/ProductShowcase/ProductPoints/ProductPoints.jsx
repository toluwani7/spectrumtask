import React from "react";
import Slider from "react-slick";
import image1 from "../../../utilities/prod1.png";
import image2 from "../../../utilities/prod2.png";
import image3 from "../../../utilities/prod3.png";
import image4 from "../../../utilities/prod4.png";
import image5 from "../../../utilities/prod5.png";
import image6 from "../../../utilities/prod6.png";

import "./ProductPoints.style.scss";
import productPoints from "../../../utilities/product-points.png";
export default function ProductPoints() {
  return (
    <div className="container">
      <div className="row">
        <div className={"col-12 product-points"}>
          <img
            src={productPoints}
            className={"img-fluid"}
            alt={"alt product"}
          />
        </div>
        <div className="col-12">
          <div className="product-points-wrapper">
            <div className="pp pp-1"></div>
            <div className="pp pp-2 pp-selected">
              <div className="pp-description">
                Lorem ipsum dolor sit amet, const adi consectetur ad ipsum dolor
                sit amet.
              </div>
            </div>
            <div className="pp pp-3"></div>
            <div className="pp pp-4"></div>
            <div className="pp pp-5"></div>
            <div className="pp pp-6"></div>
            {/* <div>
              <img src={image1} />
            </div>
            <div>
              <img src={image2} />
            </div>
            <div>
              <img src={image3} />
            </div>
            <div>
              <img src={image4} />
            </div>
            <div>
              <img src={image5} />
            </div>
            <div>
              <img src={image6} />
            </div> */}
          </div>
        </div>
      </div>
    </div>
  );
}
