import React from "react";
import horizontalBar from "../../../utilities/horizontal-bar.png";
import quickHow from "../../../utilities/quic-k-how.jpg";
import "./ProductFeatures.style.scss";
import CarouselFeature from "../../Carousel/CarouselFeature";

export default function ProductFeatures() {
  return (
    <div className="container">
      <div className="row">
        <div className={"col-12 pf-container"}>
          <div className={"col-12 pf-element"}>
            <div className={"col-12 col-md-4 pf-slider"}>
              <CarouselFeature />
            </div>
            <div
              className={"order-first order-md-last col-12 col-md-8 pf-intro"}
            >
              <div
                className={
                  "horizontal-bar top-horizontal-bar d-none d-md-block"
                }
              >
                <img src={horizontalBar} width="1px" alt={"horizontal bar"} />
              </div>
              <div className={"col-12 pf-element"}>
                <div className={"intro-element col-12 col-md-10"}>
                  <h3 className={"pf-title"}>PRODUCT INTRO</h3>
                  <p>
                    Lorem ipsum dolor sit amet, consectetur adipisicing elit,
                    sed do eiusmod tempor incididunt ut labore et dolore magna
                    aliqua. Ut enim ad minim veniam, quis nostrud exercitation
                    ullamco laboris nisi ut aliquip ex ea commodo consequat.
                    Duis aute i rure dolor in reprehenderit in voluptate velit
                    esse cillum dolore eu fugiat nulla pariatur. Excepteur sint
                    occaecat cupidatat non proident, sunt in culpa qui officia
                    deserunt mollit anim id est laborum. Sed ut perspiciatis
                    unde omnis iste natus error sit voluptatem accusantium
                    doloremque laudantium, totam rem aperiam, eaque ipsa quae ab
                    illo inventore veritatis et quasi architecto beatae vitae
                    dicta sunt explicabo. Nemo enim ipsam voluptatem quia
                    voluptas sit aspernatur aut odit aut fugit, sed quia
                    consequuntur magni dolores eos qui ratione voluptatem sequi
                    nesciunt. Neque porro quisquam est, qui dolorem ipsum quia
                    dolor sit amet, consectetur, adipisci velit, sed quia non
                    numquam eius modi tempora incidunt ut labore et dolore
                    magnam aliquam quaerat voluptatem.
                  </p>
                  <a href={"#"}>READ MORE ></a>
                </div>
                <div className={"horizontal-bar  d-none d-md-block"}>
                  <img src={horizontalBar} width="1px" alt={"horizontal bar"} />
                </div>
              </div>
              <div className={"horizontal-bar  d-none d-md-block"}>
                <img src={horizontalBar} width="1px" alt={"horizontal bar"} />
              </div>
            </div>
          </div>
          <div className={"col-12 pf-section pf-element"}>
            <div className={"col-12 pf-header-right"}>
              <h3>FEATURES SECTION</h3>
            </div>
            <div className={"col-12 pf-1 pf-element"}>
              <div className={"col-12 col-md-6"}>
                <img
                  className={"img-fluid"}
                  src={quickHow}
                  alt={"Quick How To"}
                />
              </div>
              <div className={"col-12 col-md-6 intro-element right-intro"}>
                <h3 className={"pf-header-title"}>FEATURE 1 QUICK GUIDE</h3>
                <p>
                  Lorem ipsum dolor sit amet, consectetur adipisicing elit, sed
                  do eiusmod tempor incididunt ut labore et dolore magna aliqua.
                  Ut enim ad minim veniam, quis nostrud exercitation ullamco
                  laboris nisi ut aliquip ex ea commodo consequat. Duis aute
                  irure dolor in reprehenderit in voluptate velit esse cillum
                  dolore eu fugiat nulla pariatur. Excepteur sint occaecat
                  cupidatat non proident, sunt in culpa qui officia deserunt
                  mollit anim id est laborum. Sed ut perspiciatis unde omnis
                  iste natus error sit voluptatem accusantium doloremque
                  laudantium, totam rem aperiam, eaque ipsa quae ab illo
                  inventore veritatis et quasi architecto beatae vitae dicta
                  sunt explicabo. Nemo enim ipsam voluptatem quia voluptas sit
                  aspernatur aut odit aut fugit, sed quia consequuntur magni
                  dolores eos qui ratione voluptatem sequi nesciunt. Neque porro
                  quisquam est, qui dolorem ipsum quia dolor sit amet,
                  consectetur, adipisci velit, s ed quia non numquam eius modi
                  tempora incidunt ut labore et dolore magnam aliquam quaerat
                  voluptatem.
                </p>
                <a href={"#"}>READ MORE ></a>
                <div className={"horizontal-bar d-none d-md-block"}>
                  <img src={horizontalBar} width="1px" alt={"horizontal bar"} />
                </div>
              </div>
            </div>
            <div className={"col-12 pf-2"}></div>
            <div className={"col-12 pf-3"}></div>
          </div>
        </div>
      </div>
    </div>
  );
}
