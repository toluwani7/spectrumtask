import React from "react";
import "./BottomFooter.style.scss";

export default function BottomFooter() {
  return (
    <div class="container">
      <div class="row py-3">
        <div class="col-sm-3 font-weight-bold pt-2 pb-2 d-none d-md-flex">
          COOKIE POLICY
        </div>
        <div class="col-6 col-sm-5 text-sm-center pt-2 pb-2 bf-text">
          WE USE COOKIES TO MAKE THIS WEBSITE BETTER. YOU CAN DISABLE THEM IN
          OUR COOKIE POLICY.
        </div>
        <div class="col-6 col-sm-4 pt-2 pb-2">
          <button
            type="button"
            class="float-sm-right btn btn-light ml-sm-2 mr-2 mr-sm-0 bf-button mb-2 mb-sm-0"
          >
            LEARN MORE
          </button>
          <button
            type="button"
            class="float-sm-right btn btn-danger bf-button "
          >
            ACCEPT AND CLOSE
          </button>
        </div>
      </div>
    </div>
  );
}
