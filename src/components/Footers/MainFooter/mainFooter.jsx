import React from 'react';
import './mainFooter.style.scss';

export default function MainFooter() {
  return (
    <React.Fragment>
      <footer class="pt-5 pb-4" id="contact">
        <div class="container">
          <div class="row">
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-2">
              <h5 class="mb-4 font-weight-bold">COMPANY</h5>
              <p class="mb-2">Spectrum Brands</p>
              <p class="mb-2">Sitemap</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-2">
              <h5 class="mb-4 font-weight-bold">SERVICE</h5>
              <p class="mb-2">User Manuals</p>
              <p class="mb-2">Contact Us</p>
              <p class="mb-2">Product Registration</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-2">
              <h5 class="mb-4 font-weight-bold">LEGAL</h5>
              <p class="mb-2">Privacy Policy</p>
              <p class="mb-2">Terms of use</p>
              <p class="mb-2">Imprint</p>
              <p class="mb-2">Cookie Policy</p>
            </div>
            <div class="col-lg-3 col-md-6 col-sm-6 mt-2 mb-2">
              <h5 class="mb-4 font-weight-bold">FOLLOW US</h5>
              <ul class="social-pet mt-4">
                <li>
                  <a href="#" title="instagram">
                    <i class="fab fa-youtube social-youtube"></i>
                  </a>
                </li>
                <li>
                  <a href="#" title="instagram">
                    <i class="fab fa-instagram"></i>
                  </a>
                </li>
              </ul>
            </div>
          </div>
        </div>
      </footer>
    </React.Fragment>
  );
}
