import React from 'react';
import './MainHeader.styles.scss';
import logo from '../../../utilities/remington_logo.svg';

export default function MainHeader() {
  return (
    <nav className="navbar navbar-expand-lg navbar-light bg-light">
      <a className="navbar-brand d-md-none" href="/">
        <img src={logo} />
      </a>

      <button
        className="navbar-toggler"
        type="button"
        data-toggle="collapse"
        data-target="#navbarToggle"
        aria-controls="navbarToggle"
        aria-expanded="false"
        aria-label="Toggle navigation"
      >
        <span className="navbar-toggler-icon"></span>
      </button>

      <div
        className="collapse navbar-collapse justify-content-between"
        id="navbarToggle"
      >
        <ul className="navbar-nav">
          <li className="nav-item">
            <a className="nav-link font-weight-bold active" href="/">
              MEN <span className="sr-only">(current)</span>
            </a>
          </li>
          <li className="nav-item">
            <a className="nav-link font-weight-bold" href="/">
              WOMEN
            </a>
          </li>
          <li className="nav-item font-weight-bold">
            <a className="nav-link" href="/">
              NEW ARRIVALS
            </a>
          </li>
        </ul>

        <a className="navbar-brand d-none d-lg-block" href="/">
          <img src={logo} />
        </a>

        <ul className="navbar-nav">
          <li className="nav-item font-weight-bold">
            <a className="nav-link" href="/">
              TRENDS AND TIPS
            </a>
          </li>
          <li className="nav-item font-weight-bold">
            <a className="nav-link" href="/">
              TIMELINE
            </a>
          </li>
        </ul>
      </div>
    </nav>
  );
}
