import React from "react";
import "./SellingPoints.style.scss";
import regLogo from "../../../utilities/register-icon.png";
import returnLogo from "../../../utilities/return-icon.png";

export default function SellingPoints() {
  return (
    <div className="col-12 selling-points">
      <div className="sp-wrapper">
        <span className="sp-container">
          <img className="sp-img" src={regLogo} width={15} />
          Register your product online <span className="sp-divider"> | </span>
        </span>
        <span className="sp-container">
          {" "}
          <img className="sp-img" src={returnLogo} width={20} />
          100 Days money back guarantee
        </span>
      </div>
    </div>
  );
}
