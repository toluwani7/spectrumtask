import React from "react";
import "./FindYourStory.style.scss";
import img1 from "../../utilities/fys_mattew.jpg";
import img2 from "../../utilities/fys_scarlette.jpg";
import img3 from "../../utilities/fys_rob.jpg";
import img4 from "../../utilities/fys_ava.jpg";
import img5 from "../../utilities/fys_jack.jpg";

export default function() {
  return (
    <div className="fys-wrapper">
      <div className="container">
        <div className="row">
          <div className="col-12 mt-2 mb-4">
            <hr className="page-divider mb-4 mt-4" />
          </div>
          <div className="col-12 col-sm-5">
            <h2 className="fys-header">
              Find your <span style={{ color: "red" }}>story</span>
            </h2>
            <hr className="red-divider" />
          </div>
          <div className="col-12 col-sm-7">
            <p>
              Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do
              eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut
            </p>
            <div className="small-link pt-2 mb-2">
              Explore More <i className="fa fa-long-arrow-alt-right ml-2"></i>{" "}
            </div>
          </div>
          <div className="col-12 mt-5">
            <div className="sys-wrapper">
              <div className="sys-element sys-1">
                <div className="img-link">
                  <i className="fa fa-arrow-up"></i>
                </div>
                <div className="sys-text-container">
                  <div className="sys-name">Matthew</div>
                  <div className="sys-role">Personal Trainer</div>
                </div>
                <img src={img1} alt="" />
              </div>
              <div className="sys-element sys-2">
                <img src={img2} alt="" />
              </div>
              <div className="sys-element sys-2">
                <img src={img3} alt="" />
                <div className="img-link">
                  <i className="fa fa-arrow-up"></i>
                </div>
                <div className="sys-text-container">
                  <div className="sys-name">Rob</div>
                  <div className="sys-role">Musician</div>
                </div>
              </div>
              <div className="sys-element sys-2">
                <img src={img4} alt="" />
                <div className="img-link">
                  <i className="fa fa-arrow-up"></i>
                </div>
                <div className="sys-text-container">
                  <div className="sys-name">Ava</div>
                  <div className="sys-role">Perofming Art Teacher</div>
                </div>
              </div>
              <div className="sys-element sys-2">
                <img src={img5} alt="" />
                <div className="img-link">
                  <i className="fa fa-arrow-up"></i>
                </div>
                <div className="sys-text-container">
                  <div className="sys-name">Jack</div>
                  <div className="sys-role">Mr England</div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
