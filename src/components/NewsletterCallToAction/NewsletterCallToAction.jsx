import React from "react";
import "./NewsletterCallToAction.style.scss";

export default function NewsletterCallToAction() {
  return (
    <div className={"col-12 home-heading"}>
      <div className={"dark-overlay p-2"}>
        <div className={"row"}>
          <div className={"col"}>
            <div className={"container pt-3"}>
              <div className="row">
                <div className="col-sm-6 text-center text-sm-left">
                  <h4>
                    SUBSCRIBE TO OUR <span className="nl-big">NEWSLETTER</span>
                  </h4>
                  <p className="nl-subtext">
                    Recevie Tips, Offers and Products
                  </p>
                </div>
                <div className="col-sm-6 pt-3">
                  <button
                    type="button"
                    class="btn btn-danger btn-block nl-join-btn"
                  >
                    JOIN NOW
                  </button>
                </div>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  );
}
