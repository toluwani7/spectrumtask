import React from "react";
import "./DiscoverMore.style.scss";

export default function DiscoverMore() {
  return (
    <div className="container">
      <div className="row">
        <div className={"col-12"}>
          <div
            className={
              "discover-button-wrapper text-center text-lg-right pt-4 pb-4"
            }
          >
            <button type="button" class="btn btn-discover">
              Discover More
            </button>
          </div>

          <div className={"line"}>
            <hr />
          </div>
        </div>
      </div>
    </div>
  );
}
