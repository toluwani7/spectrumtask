import React from 'react';
import TopHeader from './components/Headers/TopHeader';
import MainHeader from './components/Headers/MainHeader';
import SellingPoints from './components/Headers/SellingPoints';

import Hero from './components/Hero';
import DiscoverMore from './components/Hero/DiscoverMore';

import ProductIntroduction from './components/ProductShowcase/ProductIntroduction';
import ProductCarouselContent from './components/ProductShowcase/ProductCarouselContent';
import ProductFeatures from './components/ProductShowcase/ProductFeatures';
import ProductPoints from './components/ProductShowcase/ProductPoints';
import VideoHero from './components/VideoHero';

import FindYourStory from './components/FindYourStory';
import NewsletterCallToAction from './components/NewsletterCallToAction';

import MainFooter from './components/Footers/MainFooter';
import BottomFooter from './components/Footers/BottomFooter';

function App() {
  return (
    <React.Fragment>
      <TopHeader />
      <MainHeader />
      <SellingPoints />
      <Hero />
      <DiscoverMore />
      <ProductIntroduction />
      <VideoHero />
      <ProductCarouselContent />
      <ProductFeatures />
      <ProductPoints />
      <FindYourStory />
      <NewsletterCallToAction />
      <MainFooter />
      <BottomFooter />
    </React.Fragment>
  );
}

export default App;
